# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.select { |c| c == c.upcase }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  n = str.length
  n % 2 == 0 ? str[n/2-1..n/2] : str[n/2]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
    str.chars.select{ |c| VOWELS.include?(c) }.count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  return 1 if num == 1
  [*2..num].reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  res = ""
  arr.each.with_index { |w, i| i == arr.length - 1 ? res << w : res << "#{w}#{separator}" }
  res
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  res = ""
  str.chars.each.with_index { |c, i| res << (i % 2 != 0 ? c.upcase : c.downcase) }
  res
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like my luck has desrever")
def reverse_five(str)
  str.split(" ").map { |w| w.length >= 5 ? w.reverse : w }.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  [*1..n].reduce([]) do |res, i|
    if i % 15 == 0
      res << "fizzbuzz"
    elsif i % 5 == 0
      res << "buzz"
    elsif i % 3 == 0
      res << "fizz"
    else
      res << i
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  return true if num < 4
  [*2..num/2].each { |n| return false if num % n == 0 }
  true 
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  [*1..num].reduce([]) do |res, n| 
    num % n == 0 ? res << num / n << n : res
  end.uniq.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |n| prime?(n) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even = arr.select { |n| n % 2 == 0 }.count <= arr.length / 2
  arr.select { |n| even ? (n % 2 == 0) : (n % 2 != 0) }[0]
end
